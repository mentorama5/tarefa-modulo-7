public class ContaSalario extends Conta {
    private int limiteDeSaques;

    public ContaSalario(int numero, int agencia, String banco, double saldo, int limiteDeSaques) {
        super(numero, agencia, banco, saldo);
        this.limiteDeSaques = limiteDeSaques;
    }
    @Override
    public double getSaldo() {
        return this.saldo;
    }

    public int getLimiteDeSaques() {
        return limiteDeSaques;
    }

    @Override
    public String sacar(double valor) {
        if(limiteDeSaques>0 && (getSaldo() >= valor)) {
            setSaldo(this.saldo - valor);
            limiteDeSaques = limiteDeSaques -1;
            System.out.println("Saque executado com sucesso");
            return null;
        }
        else {
            System.out.println("Limite de saque atingido ou valor não disponível.");
        }
        return null;
    }

}
