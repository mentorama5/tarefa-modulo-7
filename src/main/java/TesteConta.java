public class TesteConta {
    public static void main(String[] args) {
        //Conta conta1 = new Conta(1,1,"DoSul",10.00);
        //System.out.print(conta1);

        //Transações na conta corrente
        ContaCorrente conta2 = new ContaCorrente(2,1, "DoSul", 100.00,
                1000.00);
        conta2.sacar(800.00);
        System.out.println("O saldo da conta corrente é R$" +conta2.getSaldo());
        System.out.println("-------");
        conta2.sacar(1500.00);
        System.out.println("O saldo da conta corrente é R$" +conta2.getSaldo());
        System.out.println("-------------------------");

        //Transações na conta poupança
        ContaPoupanca conta3 = new ContaPoupanca(3,1, "DoSul",100.00,
                20,0.05);
        System.out.println("O saldo da conta poupança é R$" +conta3.getSaldo());
        System.out.println("-------------------------");

        //Transações na conta salário
        ContaSalario conta4 = new ContaSalario(4, 1, "DoSul", 100.00,
                2);
        conta4.sacar(50);
        System.out.println("O saldo da conta salário é R$" +conta4.getSaldo());
        System.out.println("O limite de saques da conta salário é " +conta4.getLimiteDeSaques());
        System.out.println("-------");
        conta4.sacar(80);
        System.out.println("-------------------------");
    }
}
