public class ContaCorrente extends Conta {
    private double chequeEspecial;

    public ContaCorrente(int numero, int agencia, String banco, double saldo, double chequeEspecial) {
        super(numero, agencia, banco, saldo);
        this.chequeEspecial = chequeEspecial;
    }

    @Override
    public String toString() {
        return "ContaCorrente{" +
                "chequeEspecial=" + chequeEspecial +
                '}';
    }
    public double getSaldo(){
        return this.saldo;
    }
    public double getChequeEspecial() {
        return chequeEspecial;
    }

    public double limiteDeSaque() {
        return getSaldo()+getChequeEspecial();

    }

    @Override
    public String sacar(double valor) {
        if(limiteDeSaque() >= valor) {
            setSaldo(this.saldo - valor);
            System.out.println("Saque executado com sucesso");
            return null;
        }
        else {
            System.out.println("Valor não disponível para saque.");
        }
        return null;
    }

}
